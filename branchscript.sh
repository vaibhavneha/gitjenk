#!/bin/bash
action=$1
sbranch=$2
dbranch=$3

if [["${action}" == "create" ]]
then
	git branch ${sbranch}
elif [["${action}" == "list" ]]
then    
	git branch -a
elif [["${action}" == "merge" ]]
then
	git checkout ${sbranch}
	git merge ${dbranch}
elif [["${action}" == "rebase" ]]
then
	git rebase ${sbranch} ${dbranch}
elif [["${action}" == "delete" ]]
then
	git branch -d ${sbranch}

